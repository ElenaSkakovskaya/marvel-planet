import Vue from 'vue'
import VueCytoscape from 'vue-cytoscape'
import VueI18n from 'vue-i18n'
import App from './App.vue'
import 'vue-cytoscape/dist/vue-cytoscape.css'
import ru from './i18n/ru.json'
import en from './i18n/en.json'

Vue.config.productionTip = false
Vue.use(VueCytoscape)
Vue.use(VueI18n)

let locale = 'en'
if (navigator) {
  locale = navigator.language || locale
}

// Ready translated locale messages
const messages = {
  en,
  ru,
  'ru-RU': ru,
  'en-US': en,
}
// Create VueI18n instance with options
const i18n = new VueI18n({
  locale,
  messages,
})

new Vue({
  i18n,
  render: h => h(App),
}).$mount('#app')
